module com.example.jomo {
  requires javafx.controls;
  requires javafx.fxml;
  requires javafx.web;

  requires org.controlsfx.controls;
  requires org.kordamp.bootstrapfx.core;
  requires eu.hansolo.tilesfx;

  opens com.example.jomo to javafx.fxml;
  exports com.example.jomo;
}